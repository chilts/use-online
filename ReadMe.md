# useOnline #

React (State and Effect) Hook to get your online status.

## Synopsis ##

In your React component:

```
import useOnline from "use-online";

function OnlineInfo() {
  const online = useOnline();

  return <code>online = { online }</code>
}
```

There is no need to subscribe or unsubscribe to the window `'online'` or
`'offline'` events since this hook does it for you.

## Other Hooks ##

Please see all of my other React Hooks:

* [use-document-title](https://www.npmjs.com/package/use-document-title) - Change the `document.title`
* [use-window-width](https://www.npmjs.com/package/use-document-title) - Subscribe to Window 'resize' events and get the width
* [use-online](https://www.npmjs.com/package/use-online) - Get online/offline status

## Author ##

```
$ npx chilts

   ╒════════════════════════════════════════════════════╕
   │                                                    │
   │   Andrew Chilton (Personal)                        │
   │   -------------------------                        │
   │                                                    │
   │          Email : andychilton@gmail.com             │
   │            Web : https://chilts.org                │
   │        Twitter : https://twitter.com/andychilton   │
   │         GitHub : https://github.com/chilts         │
   │         GitLab : https://gitlab.org/chilts         │
   │                                                    │
   │   Apps Attic Ltd (My Company)                      │
   │   ---------------------------                      │
   │                                                    │
   │          Email : chilts@appsattic.com              │
   │            Web : https://appsattic.com             │
   │        Twitter : https://twitter.com/AppsAttic     │
   │         GitLab : https://gitlab.com/appsattic      │
   │                                                    │
   │   Node.js / npm                                    │
   │   -------------                                    │
   │                                                    │
   │        Profile : https://www.npmjs.com/~chilts     │
   │           Card : $ npx chilts                      │
   │                                                    │
   ╘════════════════════════════════════════════════════╛

```

(Ends)
